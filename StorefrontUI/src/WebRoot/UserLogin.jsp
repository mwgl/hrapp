<!DOCTYPE html>
<html>
<head>
<h1><img src="images\logo1.png" height="140" width="1273"></h1>
</head>
<style>
form {
    border: 0px solid #f1f1f1;
}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}


.imgcontainer {
    text-align: center;
    margin: 30px 300px 10px 300px;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    
    margin: 5px 300px 600px 300px;
}

span.psw {
    float: right;
    padding-top: 10px;
}

/* Change styles for span and cancel button on extra small screens */


</style>
<body #E3DEC1>

<h2 style=color:green align="center">User login</h2>

<form action="./ULoginAction" method="post">
  <div class="imgcontainer">
    <img src="images\img_avatar2.png" alt="Avatar" class="avatar">
  </div>

  <div class="container">
    <label><b>Userid</b></label>
    <input type="text" placeholder="Enter Useid" name="userid" >

    <label><b>Password</b></label>
    <input type="password" placeholder="Enter Password" name="password" >
        
    <button type="submit">Login</button>
   
 <%
    ServletContext context=getServletContext();
 
   String status=((String) context.getAttribute("status"));
		if(status!=null)  
  {
     %>
    
 <h3 style=color:red align="left"> <%=(String) context.getAttribute("status") %></h3>
    <%} %>
   
  </div>

 
</form>

</body>
</html>
