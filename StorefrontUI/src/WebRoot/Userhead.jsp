<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
.navbar {
     padding: 20px;
     overflow: visible;
     background-color: #c3b87b;
     width: 1244px;
    height: 30px;
    font-family: Arial;
}

.navbar a {
    float: left;
    font-size: 16px;
    color: white;
    text-align: center;
    padding: 12px 16px;
    text-decoration: none;
}

.dropdown {
    float: left;
    overflow: hidden;
}

.dropdown .dropbtn {
    font-size: 16px;    
    border: none;
    outline: none;
    color: white;
    padding: 14px 14px;
    background-color:#c3b87b;
    font: inherit;
    margin: 0;
}

.navbar a:hover, .dropdown:hover .dropbtn {
      background-color: #999933;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    float: none;
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {
    background-color: #c3b87b;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>

</head>
<body bgcolor="#E3DEC1" >
<img src="images\logo1.png" height="140" width="1273">
<div class="navbar">
  <a href="User.jsp">home</a>
 
  
    
    
  <a class="active" href="Myprofile.jsp">My profile</a>
  
  <div class="dropdown">
    <button class="dropbtn">request</button>
    <div class="dropdown-content">
     <a href="Requests.jsp">send requests</a>
      <a href="requestdetails.jsp">view requests</a>
     
    </div>
    </div>

 <div class="dropdown">
    <button class="dropbtn">workload</button>
    <div class="dropdown-content">
      
      <a href="Workload.jsp">send workload</a>
      <a href="Workloaddetails.jsp">view workload</a>
    </div>
    </div>
    <div>
 <a href="index.jsp">Logout</a>
 
</div>


</body>
</html></br></br>
