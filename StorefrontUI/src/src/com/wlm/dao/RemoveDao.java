package com.wlm.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.wlm.dao.Userbean;

public class RemoveDao {
	
	
	
	
	
	public int RemoveUser(Userbean ub)
	 {
		 
		 
	
	 String userid = ub.getUserid();
		
	 
	 Connection con = null;
	 PreparedStatement preparedStatement = null;
	 int i=0;
	 try
	 {
		 con = DbConnection.DbConnect();
	 String query = "delete  from userinfo where userid=?"; //Insert user details into the table 'USERS'
	 
	 preparedStatement = con.prepareStatement(query);
	 //Making use of prepared statements here to insert bunch of data
	
	 preparedStatement.setString(1, userid);
	 
	  i= preparedStatement.executeUpdate();
	 
	 }
	 catch(SQLException e)
	 {
	 e.printStackTrace();
	 }
	 
	 System.out.println(i);
	 return i;
	 }
}

