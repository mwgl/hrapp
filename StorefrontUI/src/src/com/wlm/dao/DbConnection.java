package com.wlm.dao;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnection {
	static String driver;
	static String url;
	static String username;
	static String password;
	static Connection c;
	
	
	
	static{
		
		Properties p=new Properties();
		try {
			p.load(DbConnection.class.getClassLoader().getResourceAsStream("" +"system.properties"));
		
			 driver=p.getProperty("DB_DRIVER_CLASS");
			 url=p.getProperty("DB_URL");
			 username=p.getProperty("DB_USERNAME");
			 password=p.getProperty("DB_PASSWORD");
			System.out.println("driver");
			System.out.println("username");
			System.out.println("password");
		Class.forName(driver);
		
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		 public static Connection DbConnect() throws SQLException{
			 c=DriverManager.getConnection(url, username, password);
			return c;
		}
		public static void CloseConnection() throws SQLException{
			
			if(c!=null){
				c.close();
			}
			
		}
		
		
		
		
	}
	
	
	/*public static Connection Dbconnect() {
		Properties props = new Properties();
		FileInputStream fis = null;
		Connection c = null;
		try {
			fis = new FileInputStream("system.properties");
			props.load(fis);

			// load the Driver Class
			Class.forName(props.getProperty("DB_DRIVER_CLASS"));

			// create the connection now
			c = DriverManager.getConnection(props.getProperty("DB_URL"),
					props.getProperty("DB_USERNAME"),
					props.getProperty("DB_PASSWORD"));
		} catch (Exception  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c;
	}*/
