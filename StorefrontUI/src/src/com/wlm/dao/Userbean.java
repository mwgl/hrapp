package com.wlm.dao;

public class Userbean {
	
	private String Userid;
	private String Uname;
	private String password;
	private String email;
	private String priority;
	private int priorityvalue;
	private long phoneno ;
	String querytype;
	
	String status;
	String requestid;
	
	public String getQuerytype() {
		return querytype;
	}
	public void setQuerytype(String querytype) {
		this.querytype = querytype;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getRequestid() {
		return requestid;
	}
	public void setRequestid(String requestid) {
		this.requestid = requestid;
	}


	
	public void setUname(String uname) {
		Uname = uname;
	}
	public String getUname() {
		return Uname;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPassword() {
		return password;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail() {
		return email;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriorityvalue(int priorityvalue) {
		this.priorityvalue = priorityvalue;
	}
	public int getPriorityvalue() {
		return priorityvalue;
	}
	public void setPhoneno(long phoneno) {
		this.phoneno = phoneno;
	}
	public long getPhoneno() {
		return phoneno;
	}
	
	
	
	public void setUserid(String Userid) {
		this.Userid = Userid;
	}
		
	
	public String getUserid() {
		return Userid;
	}
	
	

}
