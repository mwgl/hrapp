package com.wlm.action;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wlm.dao.DbConnection;

public class ULoginAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException
			{
		
		Connection c = null;
		try {
			c = DbConnection.DbConnect();
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	String  userid=req.getParameter("userid");
	String password=req.getParameter("password");
	
		System.out.println(userid);
		ServletContext context=getServletContext();  
		context.setAttribute("userid",userid);  
		
	
	
	PreparedStatement ps = null;
	try {
		ps = c.prepareStatement("select * from USERINFO where USERID=? and PASSWORD=?");
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	try {
		System.out.println(userid);
		ps.setString(1, userid);
		ps.setString(2, password);
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

	ResultSet rs = null;
	try {
		rs = ps.executeQuery();
	} catch (SQLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

	try {
		while (rs.next()) {
			resp.sendRedirect("User.jsp");
			return;
		}
	
		 String status="please enter a valid username and password";
		 context.setAttribute("status", status);
		System.out.println(status);

		resp.sendRedirect("UserLogin.jsp");
		
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return;
	}
}


