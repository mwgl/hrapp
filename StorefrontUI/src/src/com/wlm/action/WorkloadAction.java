package com.wlm.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wlm.dao.DbConnection;

public class WorkloadAction extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletContext context=getServletContext();
		String userid= (String) context.getAttribute("userid");
		String username= (String) context.getAttribute("username");
		System.out.println(userid);
		Connection c = null;
		try {
			c = DbConnection.DbConnect();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		 
		String priority="";
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");
		String workload = request.getParameter("workload");
		
		ResultSet rs3=null;
	 	PreparedStatement psmt = null;

	 	try {
	 		
	 			psmt = c.prepareStatement("select username,priority from USERINFO where userid=?");
	 		} 
	 	catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(userid);
		
			try {
				psmt.setString(1,userid);
				
			} catch (SQLException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
			}	
		try{
			rs3 = psmt.executeQuery();
			
		while(rs3.next()){
			  username = rs3.getString(1);
			  System.out.println(username);
		 priority = rs3.getString(2);
		 
			context.setAttribute("username",username);  
		
		 System.out.println(priority);
			
		}}
	 catch (SQLException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
		
			
			try {
				psmt = c.prepareStatement("insert into workload values(?,?,?,?)");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
			System.out.println(userid);
			System.out.println(workload);
			System.out.println(priority);
			System.out.println(username);
				psmt.setString(1,username);
				psmt.setString(2,workload);
				psmt.setString(3,priority);
				psmt.setString(4,userid);
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			

			int i=0;
			try {
				i = psmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
			if(i>0)  {
			
			
			
				RequestDispatcher rd=request.getRequestDispatcher("Workload.jsp?status=work load sent succesfully");
				rd.include(request, response);
					
				}
			else{

				RequestDispatcher rd=request.getRequestDispatcher("Workload.jsp?status=fail to send workload");
				rd.include(request, response);
}
		

		
		
	}}